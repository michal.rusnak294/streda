package main

import (
	"fmt"
	"time"
)

func main() {

	start := time.Now()

	// define variable for a and b

	a := 100000000
	b := 200

	// scan numbers entered by user

	// fmt.Print("Enter first number for a: ")
	// fmt.Scanln(&a)

	// fmt.Print("Enter first number for b: ")
	// fmt.Scanln(&b)

	/* go */
	addition(a, b)
	/* go */ multiplication(a, b)
	/* go */ devision(a, b)
	/* go */ substraction(a, b)

	time.Sleep(200 * time.Millisecond)
	elapsed := time.Since(start)
	fmt.Println("Time spent: ", elapsed)
}

func addition(a, b int) {
	result := a + b
	fmt.Printf("The sum of %d and %d is %d\n", a, b, result)
	// time.Sleep(200 * time.Millisecond)
}

func multiplication(a, b int) {
	resultMulti := a * b
	fmt.Printf("The multiplication of %d and %d is %d\n", a, b, resultMulti)
	// time.Sleep(200 * time.Millisecond)
}

func devision(a, b int) {
	resultDev := a / b
	fmt.Printf("The devision of %d and %d is %d\n", a, b, resultDev)
	// time.Sleep(200 * time.Millisecond)
}

func substraction(a, b int) {
	resultSub := a - b
	fmt.Printf("The substraction of %d and %d is %d\n", a, b, resultSub)
	// time.Sleep(200 * time.Millisecond)
}
