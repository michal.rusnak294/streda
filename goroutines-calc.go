package main

import (
	"fmt"
	"time"
)

func main() {

	start := time.Now()

	// define variable for a and b

	a := 100000000
	b := 200
	// var c, d, e, f int

	// scan numbers entered by user

	// fmt.Print("Enter first number for a: ")
	// fmt.Scanln(&a)

	// fmt.Print("Enter first number for b: ")
	// fmt.Scanln(&b)

	resultChan := make(chan int, 4)

	go addition(a, b, resultChan)
	go multiplication(a, b, resultChan)
	go devision(a, b, resultChan)
	go substraction(a, b, resultChan)

	time.Sleep(200 * time.Millisecond)
	elapsed := time.Since(start)
	fmt.Println("Time spent: ", elapsed)

	/*
		fmt.Printf("The sum of %d and %d is %d\n", a, b, result)
		fmt.Printf("The multiplication of %d and %d is %d\n", a, b, resultMulti)
		fmt.Printf("The devision of %d and %d is %d\n", a, b, resultDev)
		fmt.Printf("The substraction of %d and %d is %d\n", a, b, resultSub)
	*/

	for i := 0; i < 4; i++ {
		fmt.Println(<-resultChan)

	}
}

func addition(a, b int, ch chan int) {
	result := a + b
	ch <- result
}

func multiplication(a, b int, ch chan int) {
	resultMulti := a * b
	ch <- resultMulti
}

func devision(a, b int, ch chan int) {
	resultDev := a / b
	ch <- resultDev

}

func substraction(a, b int, ch chan int) {
	resultSub := a - b
	ch <- resultSub
}
