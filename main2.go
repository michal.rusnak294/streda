package main

import (
	"fmt"
	"time"
)

func main() {

	start := time.Now()

	// define variable for a and b

	a := 100000000
	b := 200
	// var c, d, e, f int

	// scan numbers entered by user

	// fmt.Print("Enter first number for a: ")
	// fmt.Scanln(&a)

	// fmt.Print("Enter first number for b: ")
	// fmt.Scanln(&b)

	// resultChan := make(chan int, 4)

	go addition(a, b)
	go multiplication(a, b)
	go devision(a, b)
	go substraction(a, b)

	time.Sleep(200 * time.Millisecond)
	elapsed := time.Since(start)
	fmt.Println("Time spent: ", elapsed)

	/*
		fmt.Printf("The sum of %d and %d is %d\n", a, b, result)
		fmt.Printf("The multiplication of %d and %d is %d\n", a, b, resultMulti)
		fmt.Printf("The devision of %d and %d is %d\n", a, b, resultDev)
		fmt.Printf("The substraction of %d and %d is %d\n", a, b, resultSub)
	*/
	/*
		for i := 0; i < 4; i++ {
			fmt.Println(<-resultChan)

		}
	*/
}

func addition(a, b int) {
	result := a + b
	fmt.Println(result)
}

func multiplication(a, b int) {
	resultMulti := a * b
	fmt.Println(resultMulti)
}

func devision(a, b int) {
	resultDev := a / b
	fmt.Println(resultDev)

}

func substraction(a, b int) {
	resultSub := a - b
	fmt.Println(resultSub)
}
